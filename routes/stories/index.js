import {
	withDBConnection,
	isLoggedIn
} from '../util';
import * as storyRoutes from './StoriesController';

export default app => {
	app.route('/api/stories/pictures')
		.get(isLoggedIn, withDBConnection(storyRoutes.getAllPictures));
}