import {
	getPermissions,
	getDBConnection,
	promiseQuery,
	returnSQLResponse
} from '../util';

/**
 * @description Returns all pictures either attached to a story or submitted through story capture
 */
export const getAllPictures = async (request, response, connection) => {
	const { includeStoryCapture } = request.query;

	const storyCaptureSection = includeStoryCapture ? `
		UNION
		(
			SELECT story_id, picture_url, timestamp, 'story' AS type 
			FROM califo_storycapture.Story
			WHERE picture_url IS NOT NULL AND NOT picture_url = ""
		)
	` : '';

	connection.query(
		`SELECT * FROM (
			(
				SELECT id, picture_url, timestamp, 'witnessing' AS type
				FROM rallies
				WHERE picture_url IS NOT NULL AND NOT picture_url = ""
			)
		UNION
			(
				SELECT id, picture_url, timestamp, 'rallies' AS type 
				FROM witnessing_day
				WHERE picture_url IS NOT NULL AND NOT picture_url = ""
			)
			${storyCaptureSection}
		) T
		WHERE T.timestamp IS NOT NULL
		AND NOT picture_url LIKE "https://drive.google.com%"
		ORDER BY T.timestamp DESC`,
		returnSQLResponse(connection, response, results => (
			results.map(obj => {
				if (obj.picture_url.startsWith('http')) {
					return obj;
				}
				return {
					...obj,
					picture_url: `https://drive.google.com/uc?export=view&id=${obj.picture_url.split(',')[0].trim()}`
				}
			})
		))
	)
}