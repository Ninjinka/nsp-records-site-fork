import multer from 'multer';

import {
	withDBConnection,
	isLoggedIn,
	withAdminDBConnection,
	getHomeDirectory
} from '../util';

import * as formRoutes from './FormsController';

const upload = multer({ storage: multer.memoryStorage() });

export default app => {
	app.route('/api/forms/post')
		.post(upload.any(), withAdminDBConnection(formRoutes.postGenericForm));

	app.route('/api/forms/post/witnessing')
		.post(upload.any(), withAdminDBConnection(formRoutes.postWitnessingForm));

	app.route('/api/forms/:name')
		.get(withDBConnection(formRoutes.getForm));

	app.route('/api/admin/forms/:id/activate')
		.get(isLoggedIn, withAdminDBConnection(formRoutes.activateForm));

	app.route('/api/admin/forms/:id/deactivate')
		.get(isLoggedIn, withAdminDBConnection(formRoutes.deactivateForm));

	app.route('/api/admin/forms/:id')
		.get(isLoggedIn, withDBConnection(formRoutes.getFormForEditing));

	app.route('/api/forms')
		.get(withDBConnection(formRoutes.getForms));

	// Tell the server to route forms traffic to the forms page
	app.route('/forms/*').get((request, response) => {
		response.sendFile('client/dist/forms.html', { root: getHomeDirectory() });
	});
}