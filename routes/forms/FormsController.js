import * as STATUS_CODES from 'http-status-codes';
import { getCloudinary, returnSQLResponse, getPermissions } from '../util';

export const activateForm = (request, response, connection) => {
	const { id } = request.params;
	const { isAdmin } = getPermissions(request);

	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You are not an admin user' });
	}
	
	connection.query(`
		UPDATE forms SET active = 1 WHERE id = :id
	`,
	{id},
	returnSQLResponse(connection, response, results => ({ changed: (results.changedRows > 0) })));
}

export const deactivateForm = (request, response, connection) => {
	const { id } = request.params;
	const { isAdmin } = getPermissions(request);

	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You are not an admin user' });
	}
	
	connection.query(`
		UPDATE forms SET active = 0 WHERE id = :id
	`,
	{id},
	returnSQLResponse(connection, response, results => ({ changed: (results.changedRows > 0) })));
}

export const getFormForEditing = (request, response, connection) => {
	const { id } = request.params;
	const { isAdmin } = getPermissions(request);

	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You are not an admin user' }); 
	}

	connection.query(`
		SELECT * FROM forms WHERE id = :id
	`,
	{id},
	returnSQLResponse(connection, response, results => results[0] || {}))
}

export const getForm = (request, response, connection) => {
	const { name } = request.params;

	connection.query(`
		SELECT title, subtitle, pages, creator, creator_email, custom_handler, name FROM forms
		WHERE
			name = :name AND active = 1
	`,
	{name},
	returnSQLResponse(connection, response, results => results[0] || {}))
}

export const getForms = (request, response, connection) => {
	connection.query(`
		SELECT title, id, active, creator, custom_handler, name FROM forms
	`, 
	returnSQLResponse(connection, response))
}

export const postGenericForm = (request, response, connection) => {
	const { name } = request.query;
	if (!name) {
		return response.status(STATUS_CODES.BAD_REQUEST).end();
	}
	response.status(STATUS_CODES.NOT_IMPLEMENTED).end();
}

export const postWitnessingForm = async (request, response, connection) => {
	const { files, body } = request;

	let fileUrls;
	try {
		fileUrls = await uploadImages(files);
	} catch (err) {
		return response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json(err);
	}

	const insertObj = { ...body, ...fileUrls };
	
	if (insertObj.was_location_hs) {
		insertObj.location = insertObj.location_name_hs;
	} else {
		insertObj.location = insertObj.location_name;
	}

	if (Array.isArray(insertObj.schoolreg_id)) {
		if (insertObj.schoolreg_id.length === 1) {
			insertObj.schoolreg_id = insertObj.schoolreg_id[0];
		} else {
			insertObj.multiple_schools = insertObj.schoolreg_id.join(',');
			insertObj.schoolreg_id = 905;
		}
	}
	
	return connection.query(`
	INSERT INTO witnessing_day
	(witnessing_time_spent, was_location_hs, was_debrief, training_time_spent, training_content, time_of_day, stories, spirit_filled_life, schoolreg_id, satisfied, recommittments, picture_url, others, multiple_schools, location, lifebooks, indicated_decisions, hs_student_witness, hs_student_names, gospels_of_john, gospel_presentations, gospel_booklets, follow_up_requests, debrief_explaination, date, csp_members, csp_member_names, conversation_with_christians, collected_data, chapter_id, bibles, approached)
	VALUES
	(:witnessing_time_spent, :was_location_hs, :was_debrief, :training_time_spent, :"training_content", :time_of_day, :stories, :spirit_filled_life, :schoolreg_id, :satisfied, :recommittments, :"picture_url", :others, :multiple_schools, :location, :lifebooks, :indicated_decisions, :hs_student_witness, :hs_student_names, :gospels_of_john, :gospel_presentations, :gospel_booklets, :follow_up_requests, :debrief_explaination, :date, :csp_members, :csp_member_names, :conversation_with_christians, :collected_data, :chapter_id, :bibles, :approached)`,
	insertObj,
	returnSQLResponse(connection, response));
}

const uploadImages = async files => {
	const promises = [];
	const obj = {}
	for (let i = 0; i < files.length; i++) {
		const { fieldname } = files[i];
		if (!obj[fieldname]) {
			obj[fieldname] = [];
		}
		promises.push(new Promise(async (resolve, reject) => {
			try {
				const url = await uploadImage(files[i], '/witnessing');
				obj[fieldname].push(url);
				resolve(true);
			} catch (err) {
				reject(err);
			}
		}));
	}
	try {
		await Promise.all(promises);
		return obj;
	} catch (err) {
		throw new Error(err);
	}
}

const uploadImage = async (image, folder) => {
	return new Promise((resolve, reject) => {
		const cloudinary = getCloudinary();
		cloudinary.v2.uploader.upload_stream(
			{resource_type: image.mimetype.split('/')[0], folder: `records-site${folder || ''}`}, 
			(error, result) => {
				if (error) {
					return reject(error);
				}
				return resolve(result.url);
			}
		).end(image.buffer);
	});
}