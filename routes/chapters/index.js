import {
	getChapter,
	getChapters,
	getChapterNameFromIds,
	getChapterNames,
} from './ChaptersController';

import {
	withDBConnection,
	isLoggedIn,
} from '../util';

export default app => {

	// Returns chapter names for the chapter select form component
	app.route('/api/chapters/names').get(withDBConnection(getChapterNames));

	//Returns a list of all chapters
	app.route('/api/chapters').get(isLoggedIn, withDBConnection(getChapters));
	
	//Returns a specific chapter
	app.route('/api/chapters/:id').get(isLoggedIn, withDBConnection(getChapter));

	//Returns the name of chapters from ids
	app.route('/api/chapters/:ids/name').get(isLoggedIn, withDBConnection(getChapterNameFromIds));

}
