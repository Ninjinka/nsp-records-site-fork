import {
	getPermissions,
	returnSQLResponse,
} from '../util';

//Gets all chapters from the database
export const getChapters = (request, response, connection) => {
	const { count, query } = request.query;

	const permissions = getPermissions(request);

	const queryConstraint = query ? `AND chapters.name LIKE "%${query}%"` : '';
	const countConstraint = count ? `LIMIT ${count}` : '';

	connection.query(
		`SELECT chapters.*
		FROM chapters
		WHERE (chapters.id IN ${permissions.chaptersSQL}
		OR chapters.id IN (
			SELECT chapter_id FROM school_registration WHERE school_id IN ${permissions.schoolsSQL}
		)
		OR ${permissions.hasFullAccess})
		${queryConstraint}
		ORDER BY status ASC, name ASC
		${countConstraint}`,
		returnSQLResponse(connection, response)
	);
};

export const getChapterNameFromIds = (request, response, connection) => {
	const { ids } = request.params;
	const chapterIds = ids.split(',').map(val => parseInt(val)).filter(val => val);

	connection.query(
		`SELECT
			chapters.name,
			chapters.id
		FROM
			chapters
		WHERE
			chapters.id IN (:chapterIds)
		`,
		{ chapterIds },
		returnSQLResponse(connection, response)
	);
}

export const getChapterNames = (request, response, connection) => {
	const { count, query } = request.query;

	const queryConstraint = query ? `WHERE chapters.name LIKE "%${query}%"` : '';
	const countConstraint = count ? `LIMIT ${count}` : '';

	connection.query(
		`SELECT chapters.name, chapters.id FROM chapters ${queryConstraint} ${countConstraint}`,
		returnSQLResponse(connection, response)
	)
}

export const getChapter = (request, response, connection) => {
	const {id} = request.params;

	const {
		chaptersSQL,
		schoolsSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT chapters.*
		FROM chapters
		WHERE id in(${id})
		AND (
			id in ${chaptersSQL}
			OR
			id IN (
				SELECT chapter_id FROM school_registration WHERE
					school_id IN ${schoolsSQL}
			)
			OR
			${hasFullAccess}
		)`,
		returnSQLResponse(connection, response)
	);
};
