import {
	getPermissions, getCurrentYearOfMinistry
} from '../util';
import * as STATUS_CODES from 'http-status-codes';
import moment from 'moment';

const checkSchoolExists = (id, connection, onFail, onSuccess) => {
	connection.query(`
		SELECT id FROM schools WHERE id = ${id}
	`, (err, results) => {
			if (err) {
				return onFail(err);
			}
			if (!results[0]) {
				return onFail({reason: 'School does not exist'});
			}
			onSuccess();
		})
}

const checkChapterExists = (id, connection, onFail, onSuccess) => {
	connection.query(`
		SELECT id FROM chapters WHERE id = ${id}
	`, (err, results) => {
			if (err) {
				return onFail(err);
			}
			if (!results[0]) {
				return onFail({reason: 'Chapter does not exist'});
			}
			onSuccess();
		})
}

export const enrollNewSchool = (request, response, connection) => {
	let {
		name,
		county,
		address,
		zip,
		state,
	} = request.body;

	const {
		isAdmin
	} = getPermissions(request);

	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).send('You are not an admin user!');
	}

	connection.query(`
		INSERT INTO schools 
		(name, county, address, zip, state)
		VALUES
		(:name, :county, :address, :zip, :state)
	`, 
		{name, county, address, zip, state},
		(err, results) => {
			connection.release();
			if (err) {
				return response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json({ status: 501, reason: err});
			}
			
			return response.status(STATUS_CODES.OK).json({ status: 200, id: results.insertId, name });
	});

}

export const enrollExistingSchool = (request, response, connection) => {
	let {
		schoolIds,
		startDate,
		endDate,
		chapterId,
	} = request.body;

	const {
		isAdmin
	} = getPermissions(request);

	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).send('You are not an admin user!');
	}
	
	schoolIds = schoolIds.split(',');

	const currentMinistryYear = getCurrentYearOfMinistry();

	// If we weren't given a start_date, default to begining of current ministry year
	if (!startDate) {
		startDate = currentMinistryYear.min_date;
	} else {
		startDate = moment(startDate);
	}

	// If we weren't given an end_date, default to end of current minsitry year
	if (!endDate) {
		endDate = currentMinistryYear.max_date
	} else {
		endDate = moment(endDate);
	}

	const semester = `${startDate.month() < 6 ? 'SPRING' : 'FALL'} ${startDate.year()}`;

	const queries = [], succeededSchools = [];
	schoolIds.forEach(id => {
		// Add this query to the list of queries to be executed so long as the check exists
		const query = new Promise((resolve, reject) => {
			checkSchoolExists(id, connection, reject, () => {
				checkChapterExists(chapterId, connection, reject, () => {
	
					connection.query(`
						INSERT INTO school_registration 
						(school_id, semester, chapter_id, start_date, end_date, timestamp)
						VALUES (
							${id},
							"${semester}",
							${chapterId},
							'${startDate.format('YYYY-MM-DD')}',
							'${endDate.format('YYYY-MM-DD')}',
							CURRENT_TIMESTAMP
						)
					`, (err, results) => {
						if (err) {
							console.log(err);
							return reject(err);
						}
						succeededSchools.push(id);
						return resolve(results);
					});
	
				})
			});
		});
		queries.push(query);
	});

	Promise.all(queries).then(
		() => {
			connection.release();
			response.status(STATUS_CODES.OK).json({status: 200, reason: 'Schools were successfully enrolled'});
		}
	).catch(
		() => {
			connection.release();
			response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json({status: 501, reason: 'One of the schools failed to enroll', schools: succeededSchools})
		}
	)
}