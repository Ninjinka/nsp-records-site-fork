import {
	getPermissions,
	returnSQLResponse,
	getCurrentYearOfMinistry
} from '../util';
import moment from 'moment';
import * as STATUS_CODES from 'http-status-codes';

export const getSchools = (request, response, connection) => {

	const { enrolledSchoolsOnly, showNonSchools, count, query } = request.query;
	const { min_date, max_date } = getCurrentYearOfMinistry();

	const startDate = min_date.format('YYYY-MM-DD');
	const endDate = max_date.format('YYYY-MM-DD');

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	const enrolledSchoolsOnlyConstraint = enrolledSchoolsOnly ? `
		AND id IN (
			SELECT school_id
			FROM school_registration 
			WHERE
				start_date BETWEEN '${startDate}' AND '${endDate}'
				OR
				end_date BETWEEN '${startDate}' AND '${endDate}'
		)
	` : '';

	const showNonSchoolsContraint = showNonSchools ? '' : `
			AND id NOT IN (258, 0, 238, 256)
	`;

	const queryConstraint = query ? `AND name LIKE "%${query}%"` : '';
	const countConstraint = count ? `LIMIT ${count}` : '';

	connection.query(
		`SELECT *
		FROM (
			SELECT schools.*,
			(
				SELECT chapters.name FROM school_registration JOIN chapters ON school_registration.chapter_id=chapters.id
					WHERE school_registration.school_id=schools.id
					ORDER BY start_date DESC
					LIMIT 1
			) AS 'chapter',
			(
				SELECT chapter_id FROM school_registration
					WHERE school_registration.school_id=schools.id
					ORDER BY start_date DESC
					LIMIT 1
			) AS 'chapter_id' FROM schools
			ORDER BY schools.name
		) AS t
		WHERE
			(
				chapter IN ${chaptersSQL}
				OR
				id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)
			${enrolledSchoolsOnlyConstraint}
			${showNonSchoolsContraint}
			${queryConstraint}
		${countConstraint}`,
		returnSQLResponse(connection, response)
	);
};

export const getSchoolNameFromIds = (request, response, connection) => {
	const { ids } = request.params;
	const schoolIds = ids.split(',').map(val => parseInt(val)).filter(val => val);

	connection.query(
		`SELECT
			schools.name,
			schools.id
		FROM
			schools
		WHERE
			schools.id IN (:schoolIds)
		`,
		{ schoolIds },
		returnSQLResponse(connection, response)
	);
}

export const getSchoolNames = (request, response, connection) => {
	const { enrolledSchoolsOnly, query, count } = request.query;

	const { max_date, min_date } = getCurrentYearOfMinistry();
	const enrolledSchoolsOnlyConstraint = enrolledSchoolsOnly ? 
		`(
			school_registration.start_date BETWEEN '${min_date.format('YYYY-MM-DD')}' AND '${max_date.format('YYYY-MM-DD')}' OR
			school_registration.end_date BETWEEN '${min_date.format('YYYY-MM-DD')}' AND '${max_date.format('YYYY-MM-DD')}'
		)`
		: undefined;
	
	const queryConstraint = query ? `schools.name LIKE "%${query}%"` : undefined;
	const limitConstraint = count ? `LIMIT ${count}` : '';
	const starSchoolsConstraint = 'schools.id NOT IN (238, 256, 258, 0)'

	const constraints = [ enrolledSchoolsOnlyConstraint, queryConstraint, starSchoolsConstraint ].filter(el => el).join(' AND ');

	connection.query(
		`SELECT schools.name, schools.id, school_registration.registration_id
		FROM school_registration JOIN schools ON school_registration.school_id = schools.id
		${constraints ? `WHERE ${constraints}` : '' }
		GROUP BY schools.name
		${limitConstraint}`,
		returnSQLResponse(connection, response)
	);
}

export const getSchoolsByChapterId = (request, response, connection) => {
	const {ids} = request.params;
	const { startDate, endDate, distanceOnly, allChapters } = request.query;

	const distanceQuery = distanceOnly ? 'AND school_registration.is_distance = 1' : '';
	const chaptersQuery = !allChapters ? `AND school_registration.chapter_id IN (${ids})` : '';

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT
			school_registration.school_id,
			school_registration.chapter_id,
			schools.name,
			schools.county,
			schools.address,
			schools.state,
			chapters.name as 'chapter_name'
		FROM (
			(
				school_registration
				INNER JOIN schools ON school_registration.school_id = schools.id
			)
			INNER JOIN chapters ON school_registration.chapter_id = chapters.id
		)
		WHERE
			(
				school_registration.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)
			${chaptersQuery}
			${distanceQuery}
			${startDate ? `AND start_date >= "${moment(startDate).format('YYYY-MM-DD')}"` : ''}
			${endDate ? `AND end_date <= "${moment(endDate).format('YYYY-MM-DD')}"` : ''}
		GROUP BY schools.id`,
		returnSQLResponse(connection, response)
	);
};

export const getUniqueSchoolIdsFromChapterIds = (request, response, connection) => {
	let {ids} = request.params;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT DISTINCT school_id, chapter_id 
		FROM school_registration 
		WHERE 
			chapter_id IN(${ids})
			AND (
				school_registration.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
      			${hasFullAccess}
			)`,
		returnSQLResponse(connection, response)
	);
};

export const getSchoolsNameAndIdFromSchoolRegId = (request, response, connection) => {
	let {ids} = request.params;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT schools.id, schools.name, school_registration.registration_id 
		FROM 
			schools
			INNER JOIN
			school_registration
			ON
			schools.id = school_registration.school_id
		WHERE school_registration.registration_id IN(${ids})
			AND (
				school_registration.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)`,
		returnSQLResponse(connection, response)
	);
};

export const getSchool = (request, response, connection) => {
	const {id} = request.params;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT * 
		FROM schools 
		WHERE
			id=${id}
			AND (
				(
					SELECT chapter_id FROM school_registration
						WHERE school_registration.school_id=schools.id
						ORDER BY registration_id DESC
						LIMIT 1
				) IN ${chaptersSQL}
				OR
				id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)`,
		returnSQLResponse(connection, response)
	);
};

export const getRegistrationInfoForSchool = (request, response, connection) => {
	const {id} = request.params;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess,
	} = getPermissions(request);

	connection.query(
		`SELECT schools.name, school_registration.start_date, school_registration.end_date, chapters.name AS 'chapter'
		FROM
			school_registration
			JOIN
			schools
			ON
			school_registration.school_id = schools.id
		 	JOIN chapters ON chapters.id = school_registration.chapter_id
		WHERE 
			school_registration.school_id = ${id}
			AND (
 				school_registration.chapter_id IN ${chaptersSQL}
       			OR
       			school_registration.school_id IN ${schoolsSQL}
       			OR
       			${hasFullAccess}
 			)`,
		returnSQLResponse(connection, response)
	)
}

export const getSchoolsForDate = (request, response, connection) => {
	const { isAdmin } = getPermissions(request);
	if (isAdmin)
		return response.status(STATUS_CODES.UNAUTHORIZED).send('You are not an admin user');

	let {date} = request.params;

	let dateObj = moment(date)
	let dateString1 = dateObj.year() + "-" + (dateObj.month() <= 6 ? "06-30" : "07-01")
	dateObj.subtract(1,'years').add(6,'months')
	let dateString2 = dateObj.year() + "-" + (dateObj.month() <= 6 ? "06-30" : "07-01");

	connection.query(
		`SELECT
			schools.name,
			school_registration.registration_id,
			schools.id,
			chapters.id AS chapter_id,
			chapters.name AS chapter_name
		FROM
			school_registration 
			JOIN schools 
			ON schools.id = school_registration.school_id
			JOIN chapters 
			ON chapters.id = school_registration.chapter_id
		WHERE 
			school_registration.start_date BETWEEN '${dateString1}' AND '${dateString2}'
			OR school_registration.end_date BETWEEN '${dateString1}' AND '${dateString2}'
			OR '${dateString1}' BETWEEN school_registration.start_date AND school_registration.end_date
			OR '${dateString2}' BETWEEN school_registration.start_date AND school_registration.end_date
			OR school_registration.start_date = '0000-00-00'
		GROUP BY schools.name`,
		returnSQLResponse(connection, response)
	)
}
