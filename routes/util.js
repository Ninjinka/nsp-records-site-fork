import * as STATUS_CODES from 'http-status-codes';
import mysql from 'mysql';
import moment from 'moment';
import cloudinary from 'cloudinary';

let databaseCredentials;
try {
	databaseCredentials = require('./database-credentials.json');
} catch (err) {
	databaseCredentials = {
		user: {
			user: 'califo_recsite',
			pass: 'Tilleverystudenthash3ard'
		},
		adminUrl: {
			user: 'califo_recsite',
			pass: 'Tilleverystudenthash3ard',
		},
	}
}

cloudinary.config({ 
	cloud_name: 'national-school-project', 
	api_key: '982334382457715', 
	api_secret: 'V5K9_jbZOloxl-drGV-NM-rcDJA' 
});

function typeCast(field, useDefaultTypeCasting) {
	if ((field.type === "BIT") && (field.length === 1)) {
		var bytes = field.buffer();
		return (bytes[0] === 1);
	}
	return (useDefaultTypeCasting());
}
function queryFormat (query, values) {
	if (!values) return query;
	query = query.replace(/\:"(\w+)"/g, function (txt, key) {
		console.log(key);
		if (values.hasOwnProperty(key)) {
			if (Array.isArray(values[key])) {
				return this.escape(values[key].join(','));
			}
			return `"${this.escape(values[key])}"`;
		}
		return 'NULL';
	}.bind(this));
	return query.replace(/\:(\w+)/g, function (txt, key) {
		if (values.hasOwnProperty(key)) {
			return this.escape(values[key]);
		}
		return 'NULL';
	}.bind(this));
}

let siteUrl = process.env.MODE === 'production' ? 
	'http://records.nationalschoolproject.com'
: 
	process.env.MODE === 'test' ?
		'http://test.records.nationalschoolproject.com'
	:
		`http://localhost:${process.env.RECORDS_PORT || 8080}` ;

//Initialize our database connections
const pool = mysql.createPool({
	user: databaseCredentials.user.user,
	password: databaseCredentials.user.pass,
	host: '66.35.83.208',
	database: 'califo_ministrymetrics',
	typeCast,
	queryFormat
});
const adminPool = mysql.createPool({
	user: databaseCredentials.admin.user,
	password: databaseCredentials.admin.pass,
	host: '66.35.83.208',
	database: 'califo_ministrymetrics',
	typeCast,
	queryFormat,
});
const storyCapturePool = mysql.createPool({
	user: databaseCredentials.user.user,
	password: databaseCredentials.user.pass,
	host: '66.35.83.208',
	database: 'calio_storycapture',
	typeCast,
	queryFormat,
})

const studentDevelopers = [
	"mateo.d.langstonsmith@biola.edu",
	"tjyurek@gmail.com",
	"aaron.b.chan@biola.edu",
	"enle.xu@biola.edu",
	"noa.d.wilding@biola.edu",
	"kailynne.l.lopez@biola.edu",
	"mia.m.honcoop@biola.edu",
	"joseph.william.stewart@biola.edu"
];

export const getConnectionPool = () => pool;
export const getSiteUrl = () => siteUrl;
export const getHomeDirectory = () => __dirname;
export const getCloudinary = () => cloudinary;

/***
 * @description Takes a users email and determines if that user is a student developer
 */
export const isStudentDeveloper = email => studentDevelopers.includes(email);

/***
 * @description Takes an array and formats it to be used in a SQL query
 ***/
export const formatArrayForSQL = a => {
	if (a.length == 0) {
		a.push(-1);
	}
	return JSON.stringify(a).replace("[","(").replace("]",")");
}

/***
 * @description Wraps a callback function so that it can utilize a database connection
 ***/
export const withDBConnection = callback => {
	return (request, response) => {
		try {
			pool.getConnection(
				(error, connection) => {
					if (error) {
						return response.send(error);
					}
					else {
						return callback(request, response, connection);
					}
				}
			);
		}
		catch (e) {
			response.error(e.stringify());
		}
	}
}

/**
 * @description Returns a promise that resolves to a database connection
 */
export const getDBConnection = () => {
	return new Promise((resolve, reject) => {
		try {
			pool.getConnection(
				(error, connection) => {
					if (error) {
						reject(error);
					}
					resolve(connection);
				}
			)
		} catch (err) {
			reject(err);
		}
	})
}

/***
 * @description Wraps a callback function so that it can utilize an admin database connection
 ***/
export const withAdminDBConnection = callback => {
	return (request, response) => {
		try {
			adminPool.getConnection(
				(error, connection) => {
					if (error) {
						return response.send(error);
					}
					else {
						return callback(request, response, connection);
					}
				}
			);
		}
		catch (e) {
			response.error(e.stringify());
		}
	}
}

/**
 * @description Wraps a callback function so that it can utilize a story capture database connection
 */
export const withStoryCaptureDBConnection = callback => {
	return (request, response) => {
		try {
			adminPool.getConnection(
				(error, connection) => {
					if (error) {
						return response.send(error);
					}
					else {
						return callback(request, response, connection);
					}
				}
			);
		}
		catch (e) {
			response.error(e.stringify());
		}
	}
}

/**
 * Takes a MySQL connection, SQL query, and SQL parameters and returns promisified query results
 */
export const promiseQuery = (connection, query, options) => {
	return new Promise((resolve, reject) => {
		const args = [];
		if (options) {
			args.push(options);
		}
		
		connection.query(
			query,
			...args,
			(err, results) => {
				if (err) {
					reject(err);
				}
				resolve(results);
			}
		);
	})
}

/***
 * @description Checks if user is logged in
 ***/
export const isLoggedIn = (request,response,next) => {
	if (request.isAuthenticated() || process.env.NO_AUTH) {
		return next();
	}

	response.redirect(siteUrl + '/login');
	response.status(200).end();
}

/***
 * @description Returns a JSON obeject describing which schools and chapters the user has access to
 ***/
export const getPermissions = request => {

	if (process.env.NO_AUTH) {
		return {
			chaptersSQL: '(-1)',
			schoolsSQL: '(-1)',
			chapters: [],
			schools: [],
			hasFullAccess: true,
			isAdmin: true,
		}
	}

	let chapters = request.user.chapters;
	let schools = request.user.schools;
	let hasFullAccess = request.user.hasFullAccess;
	let isAdmin = request.user.isAdmin;

	if (!hasFullAccess)
		hasFullAccess = false;

	if (chapters == undefined)
		chapters = [];

	if (schools == undefined)
		schools = [];

	return {
		chaptersSQL: formatArrayForSQL(chapters),
		schoolsSQL: formatArrayForSQL(schools),
		chapters,
		schools,
		hasFullAccess,
		isAdmin,
	};
}

/***
 * @description Handles return a callback function to be used by a SQL query
 ***/
export const returnSQLResponse = (connection, response, transformResults) => {
	return (error, results) => {
		connection.release();
		if (error) {
			console.log(error);
			response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json(JSON.stringify(error));
		} else {
			response.json(transformResults ? transformResults(results) : results);
		}
	}
}

/***
 * @description Transforms a change set (i.e. {col_to_change: new_value}) into an SQL update string
 ***/
export const getUpdateStatementFromChangeSet = changes => {
	let rval = "";
	let SQLChanges = [];

	for (let key in changes) {
		const obj = {};
		obj.key = key;
		obj.val = changes[key];
		SQLChanges.push(obj);
	}

	for (let i = 0; i < SQLChanges.length; i++) {
		rval += SQLChanges[i].key.replace(/'/g, "\\'") + " = '" + (typeof SQLChanges[i].val == "string" ? SQLChanges[i].val.replace(/'/g, "\\'") : SQLChanges[i].val) + "'"
		if (i != SQLChanges.length - 1) {
			rval += ", "
		}
	}

	return rval;
}

/***
 * @description Gets the min date and max date for the current year of ministry as moment objects
 */
export const getCurrentYearOfMinistry = () => {
	const current_date = new Date();

	let current_month = current_date.getMonth();

	let max_date;
	let min_date;

	if (current_month < 6) {
		min_date = `${current_date.getFullYear() - 1}-07-01`;
		max_date = `${current_date.getFullYear()}-06-30`;
	}
	else {
		min_date = `${current_date.getFullYear()}-07-01`;
		max_date = `${current_date.getFullYear() + 1}-06-30`;
	}

	return {
		max_date: moment(max_date),
		min_date: moment(min_date)
	};
}