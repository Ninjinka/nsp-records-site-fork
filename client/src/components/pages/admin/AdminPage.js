import React from 'react';
import IfUserIsAdmin from '../../elements/utility/if-user-is-admin/IfUserIsAdmin';
import UserIsntAdmin from './UserIsntAdmin';
import SideBar from './SideBar';

import UsersPage from './users/UsersPage';
import FormsPage from './forms/FormsPage';
import { get } from '../../../utils/fetch';

import styles from './styles.less';

export default class AdminPage extends React.Component {
	state = {
		page: 'users',
		users: [],
		forms: [],
		loadingUsers: true,
		loadingForms: true,
	}

	componentDidMount = async () => {
		this.fetchUsers();
		this.fetchForms();
	}

	fetchUsers = () => {
		this.setState({
			loadingUsers: true,
		}, async () => {
			const response = await get('/api/admin/users/list');
			this.setState({
				users: response.users,
				loadingUsers: false,
			});
		})
	}

	fetchForms = cb => {
		this.setState({
			loadingForms: true,
		}, async () => {
			const forms = await get('/api/forms');
			this.setState({ forms, loadingForms: false }, cb);
		})
	}
	
	render() {
		const { page, users, loadingUsers, forms, loadingForms } = this.state;

		return (
			<IfUserIsAdmin
				ifNotAdminComponent={(<UserIsntAdmin />)}
			>
				<div className={styles.container}>
					<SideBar
						changePage={page => this.setState({ page })}
						page={page}
					/>
					<div className={styles.content}>
						{
							page === 'users' ?
								<UsersPage
									users={users}
									loading={loadingUsers}
									refresh={this.fetchUsers}
								/>
							:
								<FormsPage
									forms={forms}
									loading={loadingForms}
									refresh={this.fetchForms}
								/>
						}
					</div>
				</div>
			</IfUserIsAdmin>
		);
	}
}