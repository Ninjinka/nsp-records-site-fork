import React from 'react';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText';

import styles from './styles.less';

export default props => (
	<div className={styles.sideBar}>
		<Paper>
			<List>
				<ListItem
					button
					selected={props.page === 'users'}
					onClick={() => props.changePage('users')}
				>
					<ListItemText>Users</ListItemText>
				</ListItem>
				<ListItem
					button
					selected={props.page === 'forms'}
					onClick={() => props.changePage('forms')}
				>
					<ListItemText>Forms</ListItemText>
				</ListItem>
			</List>
		</Paper>
	</div>
)