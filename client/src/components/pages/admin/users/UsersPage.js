import React from 'react';
import Icon from '@material-ui/core/Icon';
import Avatar from '@material-ui/core/Avatar';
import Checkbox from '@material-ui/core/Checkbox';

import PageTable from '../../../elements/page-table/PageTable';
import SuccessModal from '../../../elements/success-modal/SuccessModal';
import EnhancedToolbar from './EnhancedToolbar';
import EditUserModal from './EditUserModal';
import { post } from '../../../../utils/fetch';

import styles from '../styles.less';

export default class UsersPage extends React.Component {
	state = {
		selected: [],
		permissionsUpdating: false,
		searchValue: '',
		editingUser: null,
		dialogOpened: false,
	}

	selectAllClicked = () => {
		const { users } = this.props;
		const { selected } = this.state;
		if (users.length === selected.length) {
			this.setState({ selected: [] })
		} else {
			this.setState({ selected: users.map(user => user.email) })
		}
	}

	selectClicked = (email, event) => {
		event.stopPropagation();
		const { selected } = this.state;
		const selectedIndex = selected.indexOf(email);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, email);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(
				selected.slice(0, selectedIndex),
				selected.slice(selectedIndex + 1),
			);
		}

		this.setState({ selected: newSelected });
	}

	isSelected = email => this.state.selected.includes(email)

	getAvatar = (value, row) => {
		if (this.isSelected(row.email)) {
			return (
				<Avatar
					onClick={event => this.selectClicked(row.email, event)}
					classes={{
						root: styles.avatarSelected
					}}
				>
					<Icon>done</Icon>
				</Avatar>
			)
		}
		else if (value) {
			return (
				<Avatar
					src={value}
					classes={{
						img: styles.avatar
					}}
					onClick={event => this.selectClicked(row.email, event)}
				/>
			)
		}
		return (
			<Avatar
				onClick={event => this.selectClicked(row.email, event)}
				classes={{
					root: styles.avatar
				}}
			>
				{row.name.charAt(0)}
			</Avatar>
		)
	}

	rowIsSelected = row => this.isSelected(row.email)

	updatePermissions = async (permissions, user) => {
		this.setState({
			permissionsUpdating: true,
		}, async () => {
			console.log(permissions);
			const { selected } = this.state;
			try {
				await post('/api/admin/users/update', {
					users: !user ? selected : undefined,
					user: user || undefined,
					permissions,
				});
				this.setState({ dialogOpened: true, permissionsUpdating: false });
			} catch (err) {
				this.setState({ permissionsUpdating: false });
			}
		})
	}

	search = event => this.setState({ searchValue: event.target.value });

	filterUsers = user => user.name.toLowerCase().includes(this.state.searchValue.toLowerCase());

	openEditingModal = row => {
		const { users } = this.props;
		this.setState({
			editingUser: users.find(user => user.email === row.email)
		});
	}

	render() {
		const {
			selected,
			dialogOpened,
			permissionsUpdating,
			searchValue,
			editingUser,
		} = this.state;
		const { users, loading, refresh } = this.props;
		console.log(refresh);
		return (
			<div>
				<PageTable
					head={{
						'avatarUrl': {
							getCell: this.getAvatar,
							dense: true,
							getHeadComponent: () => (
								<Checkbox
									checked={users.length === selected.length}
									indeterminate={selected.length > 0 && selected.length < users.length}
									onChange={this.selectAllClicked}
									className={styles.checkbox}
								/>
							),
							checkbox: true
						},
						'name': {
							name: 'Name'
						},
						'role': {
							name: 'Role',
						},
						'schoolsCount': {
							name: 'Schools',
						},
						'chaptersCount': {
							name: 'Chapters',
						}
					}}
					data={users.filter(this.filterUsers)}
					Toolbar={
						<EnhancedToolbar
							numSelected={selected.length}
							updatePermissions={this.updatePermissions}
							refresh={refresh}
							search={this.search}
							searchValue={searchValue}
						/>
					}
					noMargin
					rowIsSelected={this.rowIsSelected}
					onRowClicked={this.openEditingModal}
					loading={loading || permissionsUpdating}
				/>
				<EditUserModal
					open={Boolean(editingUser)}
					onClose={() => this.setState({editingUser: null})}
					user={editingUser}
					updatePermissions={this.updatePermissions}
				/>
				<SuccessModal
					open={dialogOpened}
					onClose={() => this.setState({ dialogOpened: false }, refresh)}
					text="Permissions updated successfully!"
				/>
			</div>
		)
	}
}