import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Icon from '@material-ui/core/Icon';
import AppBarInput from '../../../elements/app-bar-input/AppBarInput';

import styles from '../styles.less';

export default class EnhancedToolbar extends React.Component {
	state = {
		anchor: null,
		filterShowing: false,
	}

	handleClick = event => {
		this.setState({ anchor: event.target });
	}

	handleClose = () => {
		this.setState({ anchor: null });
	}

	render() {
		const { anchor } = this.state;
		const {
			numSelected,
			updatePermissions,
			refresh,
			search,
			searchValue
		} = this.props;

		return (
			<Toolbar
				className={styles.toolbar}
			>
				<div className={styles.toolbarRow}>
					<Typography variant="h5">{numSelected > 0 ? `${numSelected} selected` : 'Users'}</Typography>
					<div className={styles.spacer} />
					<AppBarInput
						onChange={search}
						value={searchValue}
						placeholder="Search..."
						icon={(
							<Icon>search</Icon>
						)}
					/>
					<IconButton onClick={refresh}>
						<Icon>refresh</Icon>
					</IconButton>
					{numSelected > 0 ?
						<IconButton
							onClick={this.handleClick}
						>
							<Icon>more_vert</Icon>
						</IconButton>
						:
						null
					}
					<Menu
						open={Boolean(anchor)}
						anchorEl={anchor}
						onClose={this.handleClose}
					>
						<MenuItem
							onClick={() => updatePermissions({
								admin: true,
								has_access: true,
								has_full_access: true,
							})}
						>
							Set Role to Admin
						</MenuItem>
						<MenuItem
							onClick={() => updatePermissions({
								admin: false,
								has_access: true,
								has_full_access: true
							})}
						>
							Set Role to Full Access
						</MenuItem>
						<MenuItem
							onClick={() => updatePermissions({
								admin: false,
								has_access: true,
								has_full_access: false,
							})}
						>
							Set Role to User
						</MenuItem>
						<MenuItem
							onClick={() => updatePermissions({
								admin: false,
								has_access: false,
								has_full_access: false,
							})}
						>
							Revoke All Access
						</MenuItem>
					</Menu>
				</div>
			</Toolbar>
		)
	}
}