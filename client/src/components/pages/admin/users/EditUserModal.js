import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import FormGroup from '@material-ui/core/FormGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';

import { get } from '../../../../utils/fetch';
import MultiAutoComplete from '../../../elements/auto-complete/MultiAutoComplete';
import styles from '../styles.less';

export default class EditUserModal extends React.Component {
	state = {
		user: null,
		schools: [],
		chapters: [],
		role: '',
	}

	getAvatar = user => {
		if (user.avatarUrl) {
			return (
				<Avatar
					src={user.avatarUrl}
					classes={{
						img: styles.avatarStatic,
						root: styles.avatarStaticRoot
					}}
				/>
			)
		}
		return (
			<Avatar
				classes={{
					root: `${styles.avatarStatic} ${styles.avatarStaticRoot}`
				}}
			>
				{user.name.charAt(0)}
			</Avatar>
		)
	}

	getChapterAndSchoolNames = async () => {
		const { user } = this.state;
		if (!user || !user.permissions) {
			return this.setState({ loading: false });
		};
		const schools = user.permissions.schools || [];
		const chapters = user.permissions.chapters || [];
		const promises = [
			get(`/api/schools/${schools.join(',') || -1}/name`),
			get(`/api/chapters/${chapters.join(',') || -1}/name`)
		];
		const response = await Promise.all(promises);
		this.setState({
			schools: response[0].map(({ id, name }) => ({ value: id, label: name })),
			chapters: response[1].map(({ id, name }) => ({ value: id, label: name })),
			loading: false,
		});
	}

	initUser = () => {
		const { user } = this.state;
		if (!user) return;
		this.setState({ role: this.getRole(user) }, this.getChapterAndSchoolNames)
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.user !== this.props.user) {
			this.setState({ user: nextProps.user, loading: Boolean(nextProps.user) }, this.initUser);
		}
	}

	getRole = ({ permissions }) => {
		if (permissions.admin) {
			return 'admin';
		} else if (permissions.has_full_access) {
			return 'full_access';
		} else if (permissions.has_access) {
			return 'user';
		} else {
			return 'none';
		}
	}

	loadSchools = searchValue => {
		return get(`/api/schools?count=10&query=${searchValue}`).then(results => (
			results.map(school => ({ value: school.id, label: school.name }))
		));
	}

	loadChapters = searchValue => {
		return get(`/api/chapters?count=10&query=${searchValue}`).then(results => (
			results.map(chapter => ({ value: chapter.id, label: chapter.name }))
		));
	}

	saveButtonClicked = () => {
		const { updatePermissions, onClose } = this.props;
		const { role, chapters, user, schools } = this.state;
		let roleObject = {};
		
		if (role === 'admin') {
			roleObject = {
				admin: true,
				has_full_access: true,
				has_access: true,
			}
		} else if (role === 'full_access') {
			roleObject = {
				admin: false,
				has_full_access: true,
				has_access: true,
			}
		} else if (role === 'user') {
			roleObject = {
				admin: false,
				has_full_access: false,
				has_access: true,
			}
		} else {
			roleObject = {
				admin: false,
				has_full_access: false,
				has_access: false,
			}
		}

		updatePermissions({
			...roleObject,
			chapters: chapters.map(obj => JSON.stringify(obj.value)),
			schools: schools.map(obj => JSON.stringify(obj.value))
		}, user.email);
		onClose();
	}

	render() {
		const { open, onClose } = this.props;
		const { user, chapters, schools, role, loading } = this.state;

		if (!user) return null;

		return (
			<Dialog
				onClose={onClose}
				open={open}
				maxWidth="sm"
				fullWidth
				className={styles.dialog}
				classes={{
					paper: styles.dialogPaper
				}}
			>
				{loading ?
					<LinearProgress />
				:
					null
				}
				<DialogTitle disableTypography className={styles.dialogTitle}>
					<div className={styles.inlineGroup}>
						{this.getAvatar(user)}
						<Typography variant="h4">{user.name}</Typography>
					</div>
				</DialogTitle>
				<DialogContent className={styles.dialogContent}>
					<FormGroup className={`${styles.formRow} ${styles.formGroup}`}>
						<InputLabel htmlFor="role-select">Role</InputLabel>
						<Select
							className={styles.formCell}
							inputProps={{
								id: 'role-select'
							}}
							value={role}
							onChange={event => this.setState({ role: event.target.value })}
						>
							<MenuItem value="admin">Admin</MenuItem>
							<MenuItem value="full_access">Full Access</MenuItem>
							<MenuItem value="user">User</MenuItem>
							<MenuItem value="none">None</MenuItem>
						</Select>
					</FormGroup>
					<FormLabel className={styles.formLabel}>Approved Chapters</FormLabel>
					<FormGroup className={`${styles.formRow} ${styles.formGroup}`}>
						<FormControl fullWidth>
							<MultiAutoComplete
								onChange={chapters => this.setState({ chapters })}
								noOptions="No chapters found"
								loadAsyncOptions={this.loadChapters}
								value={chapters}
							/>
						</FormControl>
					</FormGroup>
					<FormLabel className={styles.formLabel}>Approved Schools</FormLabel>
					<FormGroup className={styles.formRow}>
						<FormControl fullWidth>
							<MultiAutoComplete
								onChange={schools => this.setState({ schools })}
								noOptions="No schools found"
								loadAsyncOptions={this.loadSchools}
								value={schools}
							/>
						</FormControl>
					</FormGroup>
				</DialogContent>
				<DialogActions>
					<Button onClick={onClose}>Cancel</Button>
					<Button
						color="primary"
						disabled={loading}
						onClick={this.saveButtonClicked}
					>
						Save
					</Button>
				</DialogActions>
			</Dialog>
		)
	}
}