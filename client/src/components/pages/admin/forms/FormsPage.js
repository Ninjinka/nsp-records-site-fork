import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';

import EnhancedToolbar from './EnhancedToolbar';
import EditFormPage from './EditFormPage';
import PageTable from '../../../elements/page-table/PageTable';

import { get } from '../../../../utils/fetch';

export default class FormsPage extends React.Component {
	state = {
		searchValue: '',
		updating: false,
		editingForm: null,
	}

	filterForms = searchValue => form => (
				!searchValue || 
				form.title.includes(searchValue) || 
				form.title.includes(searchValue)
			)

	toggleFormActive = id => event => {
		const { refresh } = this.props;
		let method;
		if (event.target.checked) {
			method = 'activate'
		} else {
			method = 'deactivate'
		}

		this.setState({
			updating: true
		}, async () => {
			await get(`/api/admin/forms/${id}/${method}`);
			refresh(() => {
				this.setState({ updating: false });
			});
		})
	}

	render() {
		const { searchValue, updating, editingForm } = this.state;
		const { forms, loading, refresh } = this.props;

		if (editingForm) {
			return (
				<EditFormPage
					formId={editingForm.id}
					onBack={() => this.setState({ editingForm: null })}
				/>
			)
		}

		return (
			<PageTable
				head={{
					'title': {
						name: 'Title'
					},
					'creator': {
						name: 'Support Person',
					},
					'name': {
						name: 'Endpoint',
						getCell: value => {
							return (
								<span>/forms/{value}</span>
							)
						}
					},
					'active': {
						name: 'Active',
						getCell: (value, row) => {
							return (
								<Checkbox checked={value} onChange={this.toggleFormActive(row.id)}/>
							)
						},
						checkbox: true,
					}
				}}
				onRowClicked={editingForm => this.setState({ editingForm })}
				loading={loading || updating}
				data={forms.filter(this.filterForms(searchValue))}
				noMargin
				Toolbar={
					<EnhancedToolbar
						searchValue={searchValue}
						search={event => this.setState({ searchValue: event.target.value })}
						refresh={refresh}
					/>
				}
			/>
		)
	}
}