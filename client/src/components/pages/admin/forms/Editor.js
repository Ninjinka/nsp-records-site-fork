import React from 'react';
import TextField from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';

import styles from './forms.less';

export default class Editor extends React.Component {
	state = {
		editing: false,
		tempValue: '',
		width: 0,
	}

	toggleEditing = () => {
		const { value } = this.props;
		this.setState(({ editing }) => ({
			editing: !editing,
			tempValue: value
		}));
	}

	acceptChanges = () => {
		const { onChange } = this.props;
		const { tempValue } = this.state;

		this.setState({
			editing: false
		}, () => {
			onChange(tempValue);
		});
	}

	setWidth = ref => {
		if (ref) {
			this.setState({
				width: ref.getBoundingClientRect().width
			});
		}
	}

	handleInputChange = event => {
		const tempValue = event.target.value
		this.setState({tempValue});
	}
	
	render() {
		const { editing, tempValue, width } = this.state;
		const { value } = this.props;

		if (!editing) {
			return (
				<span
					className={styles.editor}
					onClick={this.toggleEditing}
					ref={this.setWidth}
				>
					{value}
				</span>
			);
		}

		const safeWidth = Math.max(width, 150) + 5;

		return (
			<React.Fragment>
				<div className={styles.textFieldRoot}>
					<TextField
						placeholder="Form Title"
						className={styles.textField}
						value={tempValue}
						onChange={this.handleInputChange}
						autoFocus
						style={{ width: safeWidth }}
					/>
					<IconButton onClick={this.toggleEditing}>
						<Icon>clear</Icon>
					</IconButton>
					<IconButton onClick={this.acceptChanges}>
						<Icon>done</Icon>
					</IconButton>
				</div>
				<div style={{
					display: 'none',
					width: 'auto',
					visibility: 'hidden',
					position: 'absolute',
				}} ref={ref => this.measure = ref}>{tempValue}</div>
			</React.Fragment>
		);
	}
}