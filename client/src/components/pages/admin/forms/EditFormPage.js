import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';

import Editor from './Editor';

import { get } from '../../../../utils/fetch';

import styles from './forms.less';

export default class EditFormPage extends React.Component {
	state = {
		loading: true,
		title: '',
		name: '',
		customHandler: '',
		pages: [],
		active: false,
	}
	
	componentDidMount() {
		this.loadFormPage();
	}

	loadFormPage = async () => {
		const { formId } = this.props;
		const form = await get(`/api/admin/forms/${formId}`);
		console.log('form', form);
		if (!form.pages) return;

		this.setState({
			name: form.name,
			title: form.title,
			loading: false,
		})
	}

	handleChange = name => value => {
		this.setState({
			[name]: value
		});
	}
	
	render() {
		const {
			loading,
			name,
			title,
		} = this.state;
		const { onBack } = this.props;
		
		if (loading) {
			return (
				<div className={styles.spinnerContainer}>
					<CircularProgress />
				</div>
			)
		}

		return (
			<Paper className={styles.root}>
				<div className={styles.editPageHeader}>
					<Typography variant="h3">
						<Editor
							value={title}
							onChange={this.handleChange('title')}
						/>
					</Typography>
					<div className={styles.spacer} />
					<IconButton onClick={onBack}>
						<Icon>arrow_back</Icon>
					</IconButton>
					<IconButton>
						<Icon>refresh</Icon>
					</IconButton>
				</div>
				<Divider className={styles.divider} />
				<Typography variant="subheading">/forms/{name}</Typography>
			</Paper>
		)
	}
}