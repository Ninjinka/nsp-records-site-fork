import React, {Component} from 'react';

import moment from 'moment';
import queryString from 'query-string';

import CollapseButton from '../../elements/collapse-button/CollapseButton';

import RalliesModule from '../../elements/modules/RalliesModule';
import SchoolsModule from '../../elements/modules/SchoolsModule';
import WitnessingModule from '../../elements/modules/WitnessingModule';
import analytics from '../../../utils/analytics';
import { get } from '../../../utils/fetch';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Tooltip from '@material-ui/core/Tooltip';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';

import styles from './chapter-dashboard.css';

// Import Chapter Groupings

import CSP from '../../../constants/chapter_groupings/CSP.json';
import ISP from '../../../constants/chapter_groupings/ISP.json';

const chapter_groupings_array = [
	CSP,
	ISP
];

import {
	getCurrentYearOfMinistry
} from '../../../utils/date_calculations';

class ChapterDashboard extends Component {
	constructor(props) {
		super(props);

		const parsed = queryString.parse(props.location.search);

		let ids = [];

		let {grouping_title} = props.match.params;

		let grouping_title_from_map = false;

		if (grouping_title == 'custom') {
			ids = parsed.ids.split(',');
			grouping_title = parsed.query_title;
		}
		else if (grouping_title == 'all') {
			ids = [0];
		}
		else {
			let object = this.getTitleAndIdsForDashboard(grouping_title);
			ids = object.ids;
			grouping_title = object.grouping_title;
			grouping_title_from_map = true;
		}

		let current_ministry_year = getCurrentYearOfMinistry();

		this.state = {
			all_time_rallies_count: 0,
			all_time_witnessing_count: 0,
			chapter_ids: ids,
			chapters: [],
			collapse_all: true,
			grouping_title_from_map: grouping_title_from_map,
			grouping_title: grouping_title,
			max_date: parsed.max_date != null ? moment(parsed.max_date).format('YYYY-MM-DD') : current_ministry_year.max_date,
			min_date: parsed.min_date != null ? moment(parsed.min_date).format('YYYY-MM-DD') : current_ministry_year.min_date,
			rallies: [],
			schools: [],
			witnessing_days: [],
			distanceOnly: false,
			anchorElement: null,
			allChapters: grouping_title === 'all',
		};

		this.handleOnClick = this.handleOnClick.bind(this);
		this.handleMaxDateStateChange = this.handleMaxDateStateChange.bind(this);
		this.handleMinDateStateChange = this.handleMinDateStateChange.bind(this);
		this.handleCollapseAllState = this.handleCollapseAllState.bind(this);
	}

	handleCollapseAllState() {
		this.setState(
			{
				collapse_all: !this.state.collapse_all
			}
		)
	}

	getTitleAndIdsForDashboard(grouping_title) {
		let return_object = {};

		for (let index = 0; index < chapter_groupings_array.length; index++) {
			let chapter_grouping = chapter_groupings_array[index];

			let {title, chapters} = chapter_grouping;

			if (title.toLowerCase().replace(/ /g, '-') == grouping_title) {
				return_object.grouping_title = title

				return_object.ids = chapters.map(chapter => parseInt(chapter.id));

				break;
			}
		}

		return return_object;
	}

	handleMinDateStateChange(event) {
		return this.setState(
			{
				min_date: event.target.value
			}
		);
	}

	handleMaxDateStateChange(event) {
		return this.setState(
			{
				max_date: event.target.value
			}
		);
	}

	componentDidMount() {
		this.fetchData();
	}

	fetchData = () => {
		const {
			chapter_ids,
			min_date,
			max_date,
			distanceOnly,
			allChapters,
		} = this.state;

		const distanceOnlyString = distanceOnly ? '&distanceOnly=true' : '';
		const analyticsDistanceString = distanceOnly ? ` ${allChapters ? '' : 'AND'} distance=1` : '';
		const allChaptersString = allChapters ? '&allChapters=true' : '';
		const analyticsAllChaptersString = !allChapters ? `chapter IN (${chapter_ids})` : '';

		const analyticsWhere = (!allChapters || distanceOnly) ? `{${analyticsAllChaptersString}${analyticsDistanceString}}` : ''

		const promises = [
			get(`/api/witnessing_day/${chapter_ids}?startDate=${min_date}&endDate=${max_date}${distanceOnlyString}${allChaptersString}`),
			get(`/api/schools/chapter_ids/${chapter_ids}?startDate=${min_date}&endDate=${max_date}${distanceOnlyString}${allChaptersString}`),
			get(`/api/rallies/custom/chapter_ids/${chapter_ids}?startDate=${min_date}&endDate=${max_date}${distanceOnlyString}${allChaptersString}`),
			analytics.count(`rallies ${analyticsWhere}`),
			analytics.count(`witnessing ${analyticsWhere}`)
		]

		Promise.all(promises).then( results => {
			this.setState({
				witnessing_days: results[0],
				schools: results[1],
				rallies: results[2],
				all_time_rallies_count: results[3] && results[3].result,
				all_time_witnessing_count: results[4] && results[4].result,
			})
		});
	}

	handleOnClick() {
		this.setState(
			{
				updating_data: true
			}
		);

		this.updateDashboardComponents();
	}

	updateDashboardComponents() {
		this.fetchData();
		this.updateURLState();
		this.setState(
			{
				updating_data: false
			}
		);
	}

	updateURLState() {
		if (this.state.allChapters) {
			window.history.pushState(null, null, `/chapter/all?min_date=${this.state.min_date}&max_date=${this.state.max_date}`)
		}
		else if (this.state.grouping_title_from_map) {
			window.history.pushState(null, null, `/chapter/${this.state.grouping_title.toLowerCase().replace(/ /g, '-')}?ids=${this.state.chapter_ids}&min_date=${this.state.min_date}&max_date=${this.state.max_date}`);
		}
		else {
			window.history.pushState(null, null, `/chapter/custom?ids=${this.state.chapter_ids}&min_date=${this.state.min_date}&max_date=${this.state.max_date}&query_title=${this.state.grouping_title}`);

		}
	}

	closeMenu = () => {
		this.setState({
			anchorElement: null
		});
	}

	openMenu = event => {
		this.setState({
			anchorElement: event.currentTarget
		})
	}

	toggleDistanceOnly = () => {
		this.setState(formerState => ({
			distanceOnly: !formerState.distanceOnly
		}), this.handleOnClick);
	}

	render() {
		const { anchorElement, distanceOnly } = this.state;

		return (
			<div>
				<AppBar color="inherit" position="relative" elevation={1} className={styles.appBar}>
					<Toolbar className={styles.toolbar}>
						<Typography variant="h6" color="inherit">{
							this.state.chapters.length == 1 ?
							this.state.chapters[0].name :
							this.state.grouping_title
						}</Typography>

						<TextField
							label="Start Date"
							type="date"
							onChange={this.handleMinDateStateChange}
							value={this.state.min_date}
						/>

						<TextField
							label="End Date"
							type="date"
							onChange={this.handleMaxDateStateChange}
							value={this.state.max_date}
						/>
						
						<span>
							<Tooltip title="Refresh Dashboard">
								<IconButton
									onClick={this.handleOnClick}
								>
									<Icon>refresh</Icon>
								</IconButton>
							</Tooltip>
							<IconButton
								onClick={this.openMenu}
							>
								<Icon>more_vert</Icon>
							</IconButton>
							<Menu
								anchorEl={anchorElement}
								open={Boolean(anchorElement)}
								onClose={this.closeMenu}
							>
								<MenuItem
									onClick={this.toggleDistanceOnly}
								>
									<Typography
										variant="button"
									>
										Distance Schools Only
									</Typography>
									<Checkbox
										checked={distanceOnly}
										disableRipple
										tabIndex={-1}
									/>
								</MenuItem>
							</Menu>
						</span>
					</Toolbar>
				</AppBar>

				{
					(this.state.rallies.length > 0 || this.state.schools.length > 0 || this.state.witnessing_days.length > 0) ?

					<div className="mdl-grid mdl-textfield--align-right">
						<div className="mdl-cell--10-offset-desktop mdl-cell">
							<CollapseButton
								collapsed={this.state.collapse_all}
								collapsed_label="Show All"
								onClick={this.handleCollapseAllState}
								raised={true}
								uncollapsed_label="Hide All"
							/>
						</div>
					</div>

					:

					''
				}

				<div className="mdl-grid">
					<div className="mdl-cell mdl-cell--12-col">
						<SchoolsModule
							chapter_ids={this.state.chapter_ids}
							collapsed={this.state.collapse_all}
							max_date={this.state.max_date}
							min_date={this.state.min_date}
							schools={this.state.schools}
							sort_by="school_name"
							distanceOnly={distanceOnly}
							allChapters={this.state.allChapters}
						/>
					</div>
				</div>

				<div className="mdl-grid">
					<div className="mdl-cell mdl-cell--12-col">
						<RalliesModule
							all_time_count={this.state.all_time_rallies_count}
							collapsed={this.state.collapse_all}
							rallies={this.state.rallies}
							sort_by="school_name"
						/>
					</div>
				</div>

				<div className="mdl-grid">
					<div className="mdl-cell mdl-cell--12-col">
						<WitnessingModule
							collapsed={this.state.collapse_all}
							all_time_count={this.state.all_time_witnessing_count}
							sort_by="name"
							witnessing_days={this.state.witnessing_days}
						/>
					</div>
				</div>
			</div>
		)
	}
}

export default ChapterDashboard;
