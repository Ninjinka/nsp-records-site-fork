import React, {Component} from 'react';

import _ from 'lodash';
import moment from 'moment';
import 'whatwg-fetch';
import Card from '../card/Card';
import CollapseButton from '../collapse-button/CollapseButton';
import NumberDisplay from '../number-display/NumberDisplay';
import PageTable from '../page-table/PageTable';
import {Link} from 'react-router-dom';

import {
	setRallyIndicatedDecisions,
	setRallyTotalAttendance,
	setRallyGospelPresentations
} from '../../../utils/stateChanges';

class RalliesModule extends Component {
	constructor(props) {
		super(props);

		this.state = {
			collapsed: props.collapsed,
			rallies: props.rallies,
			reverse_sort_by: false,
			sort_by: props.sort_by,
			unique_school_array: [],
			unique_school_count: 0,
			total_attendance: 0,
			total_indicated_decisions: 0,
			total_gospel_presentations: 0,
		};

		this.handleCollapseState = this.handleCollapseState.bind(this);
		this.handleSortBy = this.handleSortBy.bind(this);
	}

	handleCollapseState() {
		this.setState(
			{
				collapsed: !this.state.collapsed
			}
		)
	}

	removeAllClassesFromElements(nodes) {
		for (let index = 0; index < nodes.length; index++) {
			let node = nodes[index];

			node.classList = '';
		}
	}

	handleSortBy(sort_by, event) {
		this.removeAllClassesFromElements(event.target.parentNode.children);

		if (sort_by == this.state.sort_by) {
			let newSortByOrder = !this.state.reverse_sort_by;

			this.setState(
				{
					reverse_sort_by: newSortByOrder,
					rallies: _.orderBy(this.state.rallies, [`${this.state.sort_by}`], [`${newSortByOrder ? 'desc' : 'asc'}`])
				}
			)

			event.target.classList.add(`mdl-data-table__header--sorted-${newSortByOrder ? 'descending' : 'ascending'}`);
		}
		else {
			this.setState(
				{
					reverse_sort_by: false,
					sort_by: sort_by,
					rallies: _.orderBy(this.state.rallies, [`${sort_by}`], [`asc`])
				}
			);

			event.target.classList.add(`mdl-data-table__header--sorted-ascending`);

		}
	}

	componentDidMount() {
		this.getUniqueSchoolCountPeriod();
		this.setState(setRallyTotalAttendance);
		this.setState(setRallyIndicatedDecisions);
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.rallies != nextProps.rallies) {
			this.setState(
				{
					rallies: _.orderBy(nextProps.rallies, [`${this.state.sort_by}`], [`${this.state.reverse_sort_by ? 'desc' : 'asc'}`])
				}
			)
		}

		if (nextProps.collapsed != this.props.collapsed) {
			this.setState(
				{
					collapsed: nextProps.collapsed
				}
			)
		}

		this.getUniqueSchoolCountPeriod();
		this.setState(setRallyTotalAttendance);
		this.setState(setRallyIndicatedDecisions);
		this.setState(setRallyGospelPresentations);
	}

	getUniqueSchoolCountPeriod() {
		let unique_school_array =  _.uniqBy(this.state.rallies, 'name');

		this.setState(
			{
				unique_school_count: unique_school_array.length,
				unique_school_array: unique_school_array
			}
		)
	}

	render() {
		return (
			<Card cssClass={this.props.cssClass}>
				<div className="mdl-grid mdl-grid-centered">
					<div className="mdl-cell mdl-cell--2-col">
						<h4>Rallies</h4>
					</div>

					<div className="mdl-cell mdl-cell--10-col mdl-grid">
						<div className="mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--12-col-tablet">
							<NumberDisplay
								label="All time rallies"
								number_display={this.props.all_time_count.toLocaleString()}
							/>
						</div>

						<div className="mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--12-col-tablet">
							<NumberDisplay
								label="Total rallies in this period"
								number_display={this.state.rallies.length.toLocaleString()}
							/>
						</div>

						<div className="mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--12-col-tablet">
							<NumberDisplay
								label="Total attendance in this period"
								number_display={this.state.total_attendance.toLocaleString()}
							/>
						</div>

						<div className="mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--12-col-tablet">
							<NumberDisplay
								label="Total decisions indicated in this period"
								number_display={this.state.total_indicated_decisions.toLocaleString()}
							/>
						</div>

						<div className="mdl-cell--3-col mdl-cell--12-col-phone mdl-cell--12-col-tablet">
							<NumberDisplay
								label="Gospel Presentations"
								number_display={this.state.total_gospel_presentations.toLocaleString()}
							/>
						</div>
					</div>
				</div>

				{
					this.state.rallies.length > 0 ?
						<div className="mdl-grid mdl-grid-centered">
							<CollapseButton
								collapsed={this.state.collapsed}
								collapsed_label="More"
								onClick={this.handleCollapseState}
								raised={false}
								uncollapsed_label="Less"
							/>

							<div className={this.state.collapsed ? "hidden" : ""}>
								<PageTable 
									cssClass="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col"
									data={this.state.rallies}
									sortable
									flat
									head={
										{
											'school_name': {
												name: 'School',
												nonNumeric: true,
												getCell: (value, row) => 
													<Link to={`/school/${row.school_id}`}>
														{value}
													</Link>
											},
											'date': {
												name: 'Date',
												getCell: (value) => moment(value.substring(0,10)).format('MMMM Do YYYY')
											},
											'total_attendance': {
												name: 'Attendance',
												isNumeric: true
											},
											'indicated_decisions': {
												name: 'Indicated Decisions',
												isNumeric: true,
											},
											'daily_theme': {
												name: 'Theme',
												nonNumeric: true
											},
											'id': {
												getCell: (value) => 
													<Link to={`/rally/${value}`}>
														<i className="material-icons">add</i>
													</Link>
											}
										}
									}
								/>
							</div>
						</div>
						:

						''
					}

			</Card>
		)
	}
}

export default RalliesModule;
