import React from 'react';

const breakpoints = {
	xs: 0,
	sm: 600,
	md: 960,
	lg: 1280,
	xl: 1920,
}

export default class MobileDisplayOnly extends React.Component {
	state = {
		onMobile: false,
	}
	
	componentDidMount() {
		window.addEventListener('resize', this.resizeCallback);
		const { breakpoint } = this.props;
		const breakpointValue = breakpoints[breakpoint] || breakpoints.sm;

		this.setState({
			onMobile: (window.innerWidth < breakpointValue)
		});
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.resizeCallback);
	}

	resizeCallback = event => {
		const { breakpoint } = this.props;
		const { onMobile } = this.state;
		const breakpointValue = breakpoints[breakpoint] || breakpoints.sm;
		
		if ((event.target.innerWidth < breakpointValue) && !onMobile) {
			this.setState({
				onMobile: true,
			});
		} else if ((event.currentTarget.innerWidth >= breakpointValue) && onMobile) {
			this.setState({
				onMobile: false,
			})
		}
	}
	
	render() {
		const { inline, desktopComponent, children, wrapperProps, ...rest } = this.props;
		const { onMobile } = this.state;

		const styles = {
			display: inline ? 'inline' : 'block'
		};

		if (!onMobile && desktopComponent) {
			<div style={styles} {...wrapperProps}>
				<desktopComponent {...rest} />
			</div>
		} else if (!onMobile) {
			return null;
		}

		return (
			<div style={styles} {...wrapperProps}>
				{children}
			</div>
		)
	}
}