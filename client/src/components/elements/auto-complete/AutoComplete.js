import React from 'react';
import Select from 'react-select';
import AsyncSelect from 'react-select/lib/Async';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import components from './components';

class AutoComplete extends React.Component {
	state = {
		stateValue: null,
		inputValue: '',
	};

	handleChange = value => {
		const { onChange } = this.props;

		if (onChange) {
			onChange(value);
		}

		this.setState({
			stateValue: value,
			inputValue: '',
		});
	};

	clearInput = () => {
		this.setState({ inputValue: '' });
	}

	render() {
		const { classes, value, theme, options, placeholder, onNew, loadAsyncOptions, ...rest } = this.props;
		const { stateValue, inputValue } = this.state;

		const selectStyles = {
			input: base => ({
				...base,
				color: theme.palette.text.primary,
				'& input': {
					font: 'inherit',
				},
			}),
		};

		let SelectComponent = Select;
		if (loadAsyncOptions) {
			SelectComponent = AsyncSelect;
		}

		return (
			<SelectComponent
				{...rest}
				classes={classes}
				styles={selectStyles}
				options={options}
				components={components}
				value={value || stateValue}
				onChange={this.handleChange}
				placeholder={placeholder}
				maxMenuHeight={200}
				menuPlacement="auto"
				onNew={onNew}
				onInputChange={inputValue => this.setState({ inputValue })}
				inputValue={inputValue}
				loadOptions={loadAsyncOptions}
				cacheOptions={Boolean(loadAsyncOptions)}
				defaultOptions={Boolean(loadAsyncOptions)}
			/>
		);
	}
}

export default withStyles(styles, { withTheme: true })(AutoComplete);