import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';

export default props => {
	const {
		text,
		open,
		onClose,
		failure
	} = props;

	return (
		<Dialog
			open={open}
			onClose={onClose}
			maxWidth="xs"
		>
			<DialogTitle>{failure ? 'Failed' : 'Success!'}</DialogTitle>
			{text &&
				<DialogContent>
					<DialogContentText>{text}</DialogContentText>
				</DialogContent>
			}
			<DialogActions>
				<Button
					onClick={onClose}
					color="primary"
					autoFocus
				>
					{failure ? 'Bummer!' : 'Yay!'}
				</Button>
			</DialogActions>
		</Dialog>
	)
}