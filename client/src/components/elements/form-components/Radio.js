import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import TextField from '@material-ui/core/TextField';

let id = 0;
const generateName = () => `form_group_${id++}`;

export default class FormRadio extends React.Component {
	state = {
		otherChecked: false,
		otherInputValue: '',
	}

	handleOtherInputChanged = event => {
		this.setState({ otherInputValue: event.target.value }, () => {
			const { onChange } = this.props;
			const { otherInputValue } = this.state;
			onChange(otherInputValue);
		});
	}
	
	mapRadioInputs = (value, index) => {
		const { otherInputValue } = this.state;
		if (value === 'other') {
			return (
				<FormControlLabel
					control={<Radio />}
					value={value}
					key={index}
					label={
						<TextField
							placeholder="Other"
							value={otherInputValue}
							onChange={this.handleOtherInputChanged}
						/>
					}
				/>
			)
		}
		return (<FormControlLabel key={index} control={<Radio />} value={value} label={value} />);
	}

	componentWillMount() {
		this.name = generateName();
	}

	handleChange = event => {
		const { onChange } = this.props;
		const { otherInputValue } = this.state;
		if (event.target.value === 'other') {
			return onChange(otherInputValue);
		}
		onChange(event.target.value);
	}
	
	render() {
		const { options, value, onChange } = this.props;

		return (
			<RadioGroup
				value={value}
				onChange={event => onChange(event.target.value)}
				name={this.name}
			>
				{options.map(this.mapRadioInputs)}
			</RadioGroup>
		)
	}
}