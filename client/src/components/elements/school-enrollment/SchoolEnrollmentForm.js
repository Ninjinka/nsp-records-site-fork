import React from 'react';
import ExistingSchoolEnrollmentForm from './ExistingSchoolEnrollmentForm';
import NewSchoolEnrollmentForm from './NewSchoolModal';

const withPersistantState = (Comp, view) => props => {
	const { saveState, changeView, ...rest } = props;

	const enhancedChangeView = (newView, newViewProps, myState) => {
		console.log(`Saving state for ${view}`);
		console.log(myState);
		saveState(view, myState, () => {
			changeView(newView, newViewProps);
		})
	}

	return (
		<Comp
			{...rest}
			changeView={enhancedChangeView}
		/>
	)
}

export default class SchoolEnrollmentForm extends React.Component {
	state = {
		view: 'existing',
		viewProps: null,
	}

	changeView = (view, viewProps) => this.setState({ view, viewProps })
	saveState = (view, state, callback) => this.setState({ [`${view}SavedState`]: state }, callback);

	render() {
		const { view, viewProps } = this.state;
		const { open, onClose } = this.props;

		const DisplayComponent = view === 'existing' ? withPersistantState(ExistingSchoolEnrollmentForm, 'existing') : withPersistantState(NewSchoolEnrollmentForm, 'new');
		const savedState = this.state[`${view}SavedState`];

		return (
			<DisplayComponent
				{...viewProps}
				savedState={savedState}
				open={open}
				onClose={onClose}
				saveState={this.saveState}
				changeView={this.changeView}
			/>
		)
	}
}