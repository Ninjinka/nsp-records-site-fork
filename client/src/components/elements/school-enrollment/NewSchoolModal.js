import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

import { post } from '../../../utils/fetch';
import styles from './styles.css';
import SuccessModal from '../success-modal/SuccessModal';
import ConfirmationModal from './ConfirmationModal';

export default class NewSchoolModal extends React.Component {
	state = {
		fullName: null,
		county: null,
		address: null,
		zip: null,
		state: null,
		successModalOpen: false,
		returnValue: null,
	}

	handleChangeFor = name => event => this.setState({ [name]: event.target.value });
	setFocus = (value) => () => this.setState({ fullNameInputFocus: value });

	addNewSchool = () => {
		const {
			fullName,
			county,
			address,
			zip,
			state,
		} = this.state;

		const {
			onClose,
		} = this.props;

		post('/api/schools/new', {
			name: fullName,
			county,
			address,
			zip,
			state,
		}).then(results => {
			this.setState({
				successModalOpen: true,
				confirmationModalOpen: false,
				resultId: results.id,
				resultName: results.name,
			});
		}).catch(value => {
			onClose();
		})
	}

	closeSuccessModal = () => {
		const { changeView } = this.props;
		this.setState({
			successModalOpen: false,
		}, () => changeView('existing', {
			defaultSchool: {
				value: this.state.resultId,
				label: this.state.resultName
			} 
		}));
	}

	render() {
		const { open, onClose, defaultName, changeView } = this.props;
		const {
			state,
			successModalOpen,
			confirmationModalOpen,
			fullName,
			fullNameInputFocus,
		} = this.state;

		return (
			<Dialog
				open={open}
				onClose={onClose}
				fullWidth
				onEnter={() => this.setState({ fullName: defaultName })}
			>
				<DialogTitle>Enroll a New School</DialogTitle>
				<DialogContent>
					<FormLabel>What is the full name of the school?</FormLabel>
					<FormGroup className={styles.formGroup}>
						<FormControl fullWidth>
							<TextField
								value={fullName}
								label="Name"
								onChange={this.handleChangeFor('fullName')}
								onFocus={this.setFocus(true)}
								onBlur={this.setFocus(false)}
								InputLabelProps={{ shrink: Boolean(fullName) || fullNameInputFocus }}
							/>
						</FormControl>
					</FormGroup>
					<FormLabel>What is the address?</FormLabel>
					<FormGroup className={styles.formGroup}>
						<FormControl fullWidth>
							<TextField
								onChange={this.handleChangeFor('address')}
								label="Address"
							/>
						</FormControl>
					</FormGroup>
					<FormLabel>What is the county, zip, and state?</FormLabel>
					<FormGroup row className={styles.row}>
						<TextField
							className={styles.rowInputBig}
							onChange={this.handleChangeFor('county')}
							label="County"
						/>
						<TextField
							className={styles.rowInput}
							onChange={this.handleChangeFor('zip')}
							label="Zip"
							type="number"
						/>
						<Select
							value={state}
							onChange={this.handleChangeFor('state')}
							className={styles.rowInput}
						>
							<MenuItem value="">None</MenuItem>
							<MenuItem value="CA">CA</MenuItem>
							<MenuItem value="IL">IL</MenuItem>
							<MenuItem value="TX">TX</MenuItem>
						</Select>
					</FormGroup>
				</DialogContent>
				<DialogActions>
					<Button onClick={() => changeView('existing', {})}>
						Back
					</Button>
					<div className={styles.flexSpacing} />
					<Button onClick={onClose}>
						Cancel
					</Button>
					<Button onClick={() => this.setState({ confirmationModalOpen: true })} color="primary">
						Add new school
					</Button>
				</DialogActions>
				<SuccessModal
					open={successModalOpen}
					onClose={this.closeSuccessModal}
					text="School was successfully added"
				/>
				<ConfirmationModal
					open={confirmationModalOpen}
					onSubmit={this.addNewSchool}
					onClose={() => this.setState({confirmationModalOpen: false })}
					text={(
						<span>Are you sure you wish to register <strong>{fullName}</strong>?</span>
					)}
				/>
			</Dialog>
		)
	}
}