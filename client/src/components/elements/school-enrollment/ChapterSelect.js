import React from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const mapChapterMenuItem = chapter => (
	<MenuItem value={chapter.value}>
		{chapter.label}
	</MenuItem>
)

export default props => {
	const {
		value,
		onChange,
		chapters
	} = props;

	return (
		<Select
			value={value}
			onChange={(event) => onChange(event.target.value)}
		>
			<MenuItem value="">
				<em>None</em>
			</MenuItem>
			{chapters.map(mapChapterMenuItem)}
		</Select>
	)
}