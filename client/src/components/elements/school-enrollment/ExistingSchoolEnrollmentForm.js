import React from 'react';

// Import Material UI Components
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

// Import custom things
import { get, post } from '../../../utils/fetch';
import MultiAutoComplete from '../auto-complete/MultiAutoComplete';
import ChapterSelect from './ChapterSelect';
import { getCurrentYearOfMinistry } from '../../../utils/date_calculations';
import ConfirmationModal from './ConfirmationModal';
import SuccessModal from '../success-modal/SuccessModal';

import styles from './styles.css';
import { withMobileDialog } from '@material-ui/core';

class SchoolEnrollmentForm extends React.Component {
	state = {
		confirmationModalOpen: false,
		successModalOpen: false,
		newSchoolModalOpen: false,
		chapters: [],
		selectedChapter: '',
		selectedSchools: [],
		startDate: '',
		endDate: '',
	}

	componentDidMount() {
		const { savedState } = this.props;
		if (savedState) {
			return this.setState(savedState);
		}

		const promises = [
			get('/api/chapters'),
		];

		Promise.all(promises).then(results => {
			this.setState({
				chapters: results[0].map(chapter => ({ value: chapter.id, label: chapter.name })),
			});
		})
	}

	loadSchools = searchValue => {
		return get(`/api/schools?count=10&query=${searchValue}`).then(results => (
			results.map(school => ({ value: school.id, label: `${school.name} (${school.state})` }))
		));
	}

	handleChangeFor = name => value => {
		this.setState({
			[name]: value,
		});
	}

	enrollSchools = () => {
		const {
			selectedChapter,
			selectedSchools,
			startDate,
			endDate,
		} = this.state;

		const {
			onClose
		} = this.props;

		this.setState({
			confirmationModalOpen: false,
		}, () => {
			post('/api/schools/enroll', {
				schoolIds: selectedSchools.map(obj => obj.value).toString(),
				chapterId: selectedChapter,
				startDate,
				endDate,
			}).then(() => {
				this.setState({
					successModalOpen: true
				});
			}).catch(() => {
				onClose();
			});
		});
	}

	closeSuccessModal = () => {
		const { onClose } = this.props;
		this.setState({
			successModalOpen: false,
		}, onClose);
	}

	setDefaultValue = () => {
		const { defaultSchool } = this.props;
		if (!defaultSchool) return;
		this.setState((prevState) => {
			const schools = prevState.selectedSchools;
			console.log(schools);
			schools.push(defaultSchool);
			return { selectedSchools: schools };
		});
	}

	render() {
		const {
			chapters,
			selectedChapter,
			confirmationModalOpen,
			selectedSchools,
			successModalOpen,
		} = this.state;

		const {
			onClose,
			open,
			fullScreen,
			changeView
		} = this.props;

		const {
			min_date,
			max_date,
		} = getCurrentYearOfMinistry();

		return (
			<Dialog
				open={open}
				onClose={onClose}
				fullWidth
				classes={{
					paper: styles.overflow
				}}
				fullScreen={fullScreen}
				onEnter={this.setDefaultValue}
			>
				<DialogTitle disableTypography>
					<Typography variant="h4">Enroll a school</Typography>
				</DialogTitle>
				<DialogContent className={styles.overflow}>
					<FormLabel className={styles.formLabel}>Which schools are you enrolling?</FormLabel>
					<FormGroup className={styles.formGroup}>
						<FormControl fullWidth>
							<MultiAutoComplete
								onChange={this.handleChangeFor('selectedSchools')}
								onNew={value => changeView('new', { defaultName: value }, this.state)}
								noOptions="No schools found"
								loadAsyncOptions={this.loadSchools}
								value={selectedSchools}
							/>
						</FormControl>
					</FormGroup>
					<FormLabel className={styles.formLabel}>What chapter is this school apart of?</FormLabel>
					<FormGroup className={styles.formGroup}>
						<FormControl>
							<ChapterSelect
								value={selectedChapter}
								chapters={chapters}
								onChange={this.handleChangeFor('selectedChapter')}
							/>
						</FormControl>
					</FormGroup>
					<FormLabel className={styles.formLabel}>What is the start and end date?</FormLabel>
					<FormGroup row className={`${styles.row} ${styles.formGroup}`}>
						<TextField
							className={styles.rowInput}
							defaultValue={min_date}
							label="Start"
							type="date"
							onChange={(event) => this.handleChangeFor('startDate')(event.target.value)}
						/>
						<TextField
							className={styles.rowInput}
							defaultValue={max_date}
							label="End"
							type="date"
							onChange={(event) => this.handleChangeFor('endDate')(event.target.value)}
						/>
					</FormGroup>
				</DialogContent>
				<DialogActions>
					<Button onClick={onClose}>
						Cancel
					</Button>
					<Button
						onClick={() => this.handleChangeFor('confirmationModalOpen')(true)}
						color="primary"
					>
						Submit
					</Button>
				</DialogActions>
				<ConfirmationModal
					open={confirmationModalOpen}
					onSubmit={this.enrollSchools}
					onClose={() => this.handleChangeFor('confirmationModalOpen')(false)}
					selectedSchools={selectedSchools}
				/>
				<SuccessModal
					open={successModalOpen}
					onClose={this.closeSuccessModal}
					text="Schools were successfully registered"
				/>
			</Dialog>
		)
	}
}

export default withMobileDialog({breakpoint: 'xs'})(SchoolEnrollmentForm);