import React from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import LinearProgress from '@material-ui/core/LinearProgress';
import Button from '@material-ui/core/Button';

export default ({ open, onConfirm, onReject, text, title, confirmText, rejectText, loading }) => (
	<Dialog
		open={open}
		onClose={onReject}
	>
		{loading ? <LinearProgress /> : null}
		<DialogTitle>{title || 'Are you sure?'}</DialogTitle>
		<DialogContent>
			<DialogContentText>{text || 'Do you wish to perform this action'}</DialogContentText>
		</DialogContent>
		<DialogActions>
			<Button onClick={onReject}>{rejectText || 'No'}</Button>
			<Button color="primary" onClick={onConfirm}>{confirmText || 'Yes'}</Button>
		</DialogActions>
	</Dialog>
)