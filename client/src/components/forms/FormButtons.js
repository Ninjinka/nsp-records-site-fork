import React from 'react';
import Button from '@material-ui/core/Button';

import styles from './app.less';

export default ({ currentPage, pageCount, previousPage, nextPage, submit }) => (
	<React.Fragment>
		{currentPage !== 0 &&
		<Button
			color="primary"
			onClick={previousPage}
			variant="outlined"
			className={styles.previousButton}
		>
			Previous Page
		</Button>
		}
		{currentPage < pageCount - 1 &&
		<Button
			color="primary"
			onClick={nextPage}
			variant="outlined"
		>
			Next Page
		</Button>
		}
		{currentPage === pageCount - 1 && 
		<Button
			color="primary"
			onClick={submit}
			variant="outlined"
		>
			Submit
		</Button>}
	</React.Fragment>
)