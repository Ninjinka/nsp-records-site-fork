const parenthesisExtractor = /(\([^\)\(]+\))+/g;

const tryParse = value => {
	try {
		return JSON.parse(value);
	} catch (e) {
		return undefined;
	}
}

const getValue = (value, values) => {
	const parsedVar = tryParse(values[value]);
	if (parsedVar !== undefined) return parsedVar;

	const parsedValue = tryParse(value);
	if (parsedValue !== undefined) return parsedValue;
	
	return value;
}

const operators = {
	'=': (left, right, values) => {
		const leftVal = getValue(left, values);
		const rightVal = getValue(right, values);

		return leftVal === rightVal;
	},

	'>': (left, right, values) => {
		const leftVal = getValue(left, values);
		const rightVal = getValue(right, values);

		return leftVal > rightVal;
	},

	'<': (left, right, values) => {
		const leftVal = getValue(left, values);
		const rightVal = getValue(right, values);

		return leftVal < rightVal;
	}, 

	// The left hand operand for the 'is' keyword should be a variable name
	// The right hand operand should be some abstract description that doesn't make
	// sense to use an equality operator with (E.g. blank, even, odd, natural, etc)
	'is': (left, right, values) => {
		const leftVal = tryParse(values[left]);

		if (right === 'blank') {
			return leftVal === null || leftVal === undefined;
		}
	}
}

// Function: evaluateCondition boolean conditional statement
export const evaluateCondition = (condition, values) => {
	let statement = condition.replace(parenthesisExtractor, subStatement => evaluateCondition(subStatement.substring(1, subStatement.length - 1), values));
	let statementMap = statement.split(/or/i).map(subStatement => subStatement.split(/and/i));
	let orResult = false;
	for (let i = 0; i < statementMap.length; i++) {
		let andResult = true;
		for (let j = 0; j < statementMap[i].length; j++) {

			const expressionParts = statementMap[i][j].split(' ').filter(val => val);
			if (expressionParts.length === 1) {
				andResult = Boolean(tryParse(expressionParts[0]) || tryParse(values[expressionParts[0]]));
				if (!andResult) {
					break;
				}
			} else if (expressionParts.length === 3 && operators[expressionParts[1].toLowerCase()]) {

				const [ left, op, right ] = expressionParts;
				andResult = operators[op.toLowerCase()](left, right, values);

				if (!andResult) {
					break;
				}

			} else {
				andResult = false;
				break;
			}

		}
		orResult = andResult;
		if (orResult) {
			return true;
		}
	}
	return false;
}

const getAllQuestions = pages => pages.reduce((acc, val) => [...acc, ...val.questions], []);

export const getInputsThatTriggerUpdate = pages => {
	const allQuestions = getAllQuestions(pages);
	const names = new RegExp(allQuestions.map(res => `\\b${res.name}\\b`).join('|'), 'g');
	const conditions = allQuestions.map(res => res.condition).filter(val => val);
	return conditions.reduce((acc, value) => [...acc, ...value.match(names)], []).filter((v, i, a) => a.indexOf(v) === i);
}

export const cleanSubmission = (values, pages) => {
	const allQuestions = getAllQuestions(pages);
	const conditionQuestions = allQuestions.map(val => val.condition && val).filter(val => val);
	const hiddenQuestions = conditionQuestions.map(question => !evaluateCondition(question.condition, values) ? question.name : undefined).filter(val => val);
	const nullifier = hiddenQuestions.reduce((acc, val) => ({ ...acc, [val]: undefined }), {});
	const nullifiedValues = { ...values, ...nullifier };
	const cleansedValues = Object.keys(nullifiedValues).filter(key => nullifiedValues[key] !== undefined).reduce((acc, val) => ({ ...acc, [val]: nullifiedValues[val] }), {});
	return cleansedValues;
}

export const getEmptyRequiredInputs = (values, questions) => (
	questions
		// Get required questions
		.filter(question => question.required)
		// From the required questions, nullify the ones that have non-null, non-undefined values
		.map(
			question => (
					(values[question.name] === undefined || values[question.name] === null || (Array.isArray(values[question.name]) && values[question.name].length === 0) )
					&& (!question.condition || evaluateCondition(question.condition, values))
				) ? 
					question.name 
				: 
					undefined
		)
		// Filter out falsy values
		.filter(val => val)
)