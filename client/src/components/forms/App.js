import React from 'react';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import Paper from '@material-ui/core/Paper';
import DialogActions from '@material-ui/core/DialogActions';
import CircularProgress from '@material-ui/core/CircularProgress';

import FileUpload from '../elements/form-components/FileUpload';
import ChapterSelect from '../elements/form-components/ChapterSelect';
import Bool from '../elements/form-components/Boolean';
import SchoolSelect from '../elements/form-components/SchoolSelect';
import Radio from '../elements/form-components/Radio';
import Checkbox from '../elements/form-components/Checkbox';
import ConfirmDialog from './ConfirmationDialog';
import SuccessModal from '../elements/success-modal/SuccessModal';

import { evaluateCondition, getInputsThatTriggerUpdate, cleanSubmission, getEmptyRequiredInputs } from './ConditionalEngine';

import { postForm, get } from '../../utils/fetch';

import styles from './app.less';
import FormButtons from './FormButtons';

const components = {
	'text': props => (<TextField {...props} onChange={event => props.onChange(event.target.value)} />),
	'long_text': props => (<TextField multiline {...props} onChange={event => props.onChange(event.target.value)} />),
	'number': props => (<TextField type="number" {...props} onChange={event => props.onChange(event.target.value)} />),
	'file': FileUpload,
	'chapter_select': ChapterSelect,
	'boolean': Bool,
	'school_select': SchoolSelect,
	'date': props => (<TextField {...props} type="date" onChange={event => props.onChange(event.target.value)} value={props.value} />),
	'radio': Radio,
	'checkbox': Checkbox,
}

export default class App extends React.Component {

	state = {
		loading: true,
		title: 'Loading...',
		pages: [],
		customHandler: '',
		componentsThatTriggerUpdate: [],
		name: '',
		errorInputs: [],
		confirmDialogOpen: false,
		dialogLoading: false,
		values: {},
		successModalOpen: false,
		requestFailed: false,
		cleanedValues: {},
		currentPage: 0,
		notFound: false,
	}

	componentDidMount = async () => {
		try {
			const { name } = this.props.match.params;
			const response = await get(`/api/forms/${name}`);
			if (!response.pages) {
				return this.setState({ notFound: true, loading: false });
			}
			response.pages = JSON.parse(response.pages);
			this.setState({
				pages: response.pages,
				componentsThatTriggerUpdate: getInputsThatTriggerUpdate(response.pages),
				title: response.title,
				customHandler: response.custom_handler,
				name: response.name,
				loading: false,
			})
		} catch (err) {
			console.log(err);
			this.setState({
				loading: false,
				notFound: true,
			});
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (this.changeNotInValues(this.state, nextState)) {
			return true;
		}

		const { componentsThatTriggerUpdate } = nextState;

		return Boolean(componentsThatTriggerUpdate.find(
			value => {
				const oldValue = this.state.values[value];
				const newValue = nextState.values[value];
				if (JSON.stringify(oldValue) !== JSON.stringify(newValue)) {
					return true;
				}
			}
		));
	}

	changeNotInValues = (oldState, newState) => {
		return Boolean(Object.keys(oldState).find(val => val !== 'values' && JSON.stringify(oldState[val]) !== JSON.stringify(newState[val])));
	}

	handleChange = name => value => {
		this.setState(prev => ({
			values: {
				...prev.values,
				[name]: value
			}
		}));
	}

	getFormComponent = (type, name, options, defaultValue) => {
		if (typeof type !== 'string') {
			return (<span>No type defined</span>)
		}
		const typeSplit = type.split(':');
		const typeName = typeSplit.shift();
		const componentProps = typeSplit.reduce((acc, prop) => {
			const sides = prop.split('=');
			if (!sides[0]) return acc;
			if (sides[1]) {
				try {
					acc[sides[0]] = JSON.parse(sides[1]);
				} catch (err) {

				}
			} else {
				acc[sides[0]] = true
			}
			return acc;
		}, {})

		const Component = components[typeName] || (() => (<span>No component defined for type {typeName}</span>))
		return (
			<Component
				{...componentProps}
				onChange={this.handleChange(name)}
				options={options}
				defaultValue={defaultValue}
			/>
		)
	}

	mapFormQuestion = question => {
		const { errorInputs } = this.state;
		return (
			<div
				key={question.name}
				className={styles.question}
				style={{
					display: question.condition && !evaluateCondition(question.condition, this.state.values) ? 'none' : undefined
				}}
				id={question.name}
			>
				<Typography
					variant="h5"
					className={`
						${styles.questionTitle}
						${!question.subtitle ? styles.questionSubtitle : ''}
						${errorInputs.includes(question.name) ? styles.questionError : ''}
						${question.required ? styles.questionRequired : ''}
					`}
				>
					{question.title}
				</Typography>
				{question.subtitle ?
					<Typography
						variant="subtitle2"
						className={styles.questionSubtitle}
					>
						{question.subtitle}
					</Typography>
					:
					null
				}
				<FormControl fullWidth>
					{this.getFormComponent(question.type, question.name, question.options, question.default)}
				</FormControl>
			</div>
		)
	}

	checkRequiredInputs = () => {
		const { values, pages, currentPage } = this.state;
		const errorInputs = getEmptyRequiredInputs(values, pages[currentPage].questions);

		if (errorInputs.length !== 0) {
			this.setState({ errorInputs }, () => {
				const { errorInputs } = this.state;
				const el = document.getElementById(errorInputs[0]);
				if (el) {
					el.scrollIntoView({ behavior: 'smooth' })
				}
			});
			return false;
		}
		return true;
	}

	submit = () => {
		const { values, pages } = this.state;
		const cleanedSubmission = cleanSubmission(values, pages);

		if (!this.checkRequiredInputs()) {
			return;
		}

		this.setState({ errorInputs, confirmDialogOpen: true, cleanedValues: cleanedSubmission });
	}

	nextPage = () => {
		if (!this.checkRequiredInputs()) {
			return;
		}
		this.setState(({ currentPage }) => ({
			currentPage: currentPage - 1
		}))
	}

	confirmSubmission = () => {
		this.setState({ dialogLoading: true }, async () => {
			const { cleanedValues, name, customHandler } = this.state;

			try {
				const url = customHandler ? `/api/forms/post/${customHandler}?name=${name}` : `/api/forms/post?name=${name}`
				const success = await postForm(url, cleanedValues);

				this.setState({
					dialogLoading: false,
					confirmDialogOpen: false,
					successModalOpen: true,
					requestFailed: !success
				});
			} catch (err) {
				this.setState({
					dialogLoading: false,
					confirmDialogOpen: false,
					successModalOpen: true,
					requestFailed: true
				});
			}
		});
	}

	mapPages = (page, index) => {
		const { currentPage } = this.state;

		return (
			<div style={{ display: index !== currentPage ? 'none' : undefined }} key={index}>
				{page.questions && page.questions.map(this.mapFormQuestion)}
			</div>
		)
	}

	render() {
		const {
			title,
			pages,
			confirmDialogOpen,
			dialogLoading,
			requestFailed,
			successModalOpen,
			loading,
			currentPage,
			notFound
		} = this.state;

		return (
			<div className={styles.container}>
				<Paper className={styles.paper}>
					<Typography
						variant="h3"

						className={styles.formTitle}
					>
						{notFound ? 'Form Not Found :(' : title || 'Loading...'}
					</Typography>
					{loading ?
						<div className={styles.loadingSpinner}>
							<CircularProgress />
						</div>
						:
						pages.map(this.mapPages)
					}
					{notFound ?
						<div className={styles.notFound}>
							<Typography variant="h5">It may be deactivated or non-existent. Try checking the url, refreshing the page, or emailing <a href="mailto:joseph.s@nationalschoolproject.com">IT</a>.</Typography>
						</div>
					: null
					}
					<DialogActions>
						<FormButtons
							previousPage={() => this.setState(({ currentPage }) => ({ currentPage: currentPage - 1 }))}
							nextPage={this.nextPage}
							currentPage={currentPage}
							pageCount={pages.length}
							submit={this.submit}
						/>
					</DialogActions>
				</Paper>
				<ConfirmDialog
					open={confirmDialogOpen}
					onReject={() => this.setState({ confirmDialogOpen: false })}
					onConfirm={this.confirmSubmission}
					text="Do you wish to submit this form?"
					confirmText="Submit"
					rejectText="Cancel"
					title="Submit Form"
					loading={dialogLoading}
				/>
				<SuccessModal
					open={successModalOpen}
					failure={requestFailed}
					onClose={() => this.setState({ successModalOpen: false })}
					text={requestFailed ? 'The form could not be submitted, please email joseph.s@nationalschoolproject' : 'The form was successfully submitted!'}
				/>
			</div>
		)
	}
}