import moment from 'moment';

let toggleIsLoading = (state, props) => {
	return {
		loading: !state.loading
	}
}

let handleInitialIdState = (state, props) => {
	let {id} = props.match.params;

	return {
		id: id
	}
}

let setRallyTotalAttendance = (state, props) => {
	let value = 0;

	state.rallies.forEach(
		rally => {
			value = value + rally.total_attendance;
		}
	);

	return {
		total_attendance: value
	}
}

let setRallyGospelPresentations = (state,props) => {
	let value = 0;

	state.rallies.forEach(
		rally => {
			if (rally.speaker_gospel_shared != null) {
				value += rally.speaker_gospel_shared.toLowerCase() == 'yes' ? 1 : 0
			}
		}
	)

	return {
		total_gospel_presentations: value
	}
}

let setRallyIndicatedDecisions = (state, props) => {
	let value = 0;

	state.rallies.forEach(
		rally => {
			value = value + rally.indicated_decisions;
		}
	);

	return {
		total_indicated_decisions: value
	}
}

let setWitnessingDayIndicatedDecisions = (state, props) => {
	let value = 0;

	state.witnessing_days.forEach(
		witnessing_day => {
			value = value + witnessing_day.indicated_decisions;
		}
	);

	return {
		total_indicated_decisions: value
	}
};

let setWitnessingDayGospelPresentations = (state, props) => {
	let value = 0;

	state.witnessing_days.forEach(
		witnessing_day => {
			value = value + witnessing_day.gospel_presentations;
		}
	);

	return {
		total_gospel_presentations: value
	}
};

let setRallyTotalStudentsParticipating = (state, props) => {
	let value = 0;

	state.rallies.forEach(
		rally => {
			value = value + rally.students_participating;
		}
	);

	return {
		students_participating: value
	}
}

let handleInitialEndDateState = (state, props) => {
	let {end_date} = props.match.params;
	let date_from_url = true;

	let dateValue = new Date(end_date);

	if (isNaN(dateValue.valueOf())) {
		dateValue = new Date();

		date_from_url = false;
	}

	dateValue = moment(dateValue).format('YYYY-MM-DD');

	return {
		date_from_url: date_from_url,
		end_date: dateValue
	}
}

let handleInitialStartDateState = (state, props) => {
	let {start_date} = props.match.params;
	let date_from_url = true;

	let dateValue = new Date(start_date);

	if (isNaN(dateValue.valueOf())) {
		date_from_url = false;
		dateValue = new Date('2004-01-02');
	}

	dateValue = moment(dateValue).format('YYYY-MM-DD');

	return {
		date_from_url: date_from_url,
		start_date: dateValue
	}
}

export {
	handleInitialEndDateState,
	handleInitialIdState,
	handleInitialStartDateState,
	setRallyTotalAttendance,
	setRallyIndicatedDecisions,
	setRallyTotalStudentsParticipating,
	setWitnessingDayGospelPresentations,
	setWitnessingDayIndicatedDecisions,
	setRallyGospelPresentations,
	toggleIsLoading
};
