import 'babel-polyfill';

import ReactDOM from 'react-dom';
import React from 'react';
import {Route, BrowserRouter, Switch} from 'react-router-dom';
import App from './components/forms/App';

import { createGenerateClassName, jssPreset, createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import indigo from '@material-ui/core/colors/indigo';
import orange from '@material-ui/core/colors/orange';
import JssProvider from 'react-jss/lib/JssProvider';
import { create } from 'jss';

const theme = createMuiTheme({
	palette: {
		primary: indigo,
		secondary: orange,
	}
});

const generateClassName = createGenerateClassName();
const jss = create({
	...jssPreset(),
	insertionPoint: 'jss-insertion-point',
});

ReactDOM.render(
	<JssProvider jss={jss} generateClassName={generateClassName}>
		<MuiThemeProvider theme={theme}>
			<BrowserRouter forceRefresh={true}>
				<Switch>
					<Route path="/forms/:name" component={App}/>
					<Route path="/forms" component={() => (<span>Not yet implemented</span>)} />
				</Switch>
			</BrowserRouter>
		</MuiThemeProvider>
	</JssProvider>,
	document.getElementById('content')
)